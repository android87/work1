package Kotlin_JS

external fun alert(msg: String)   // 1

fun main() {
    alert("Hi!")                    // 2
}