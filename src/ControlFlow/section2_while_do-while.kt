package ControlFlow

fun eatCake() = println("Eat a Cake")
fun bakeCake() = println("Bake a cake")

fun main () {
    var cakesEaten: Int = 0
    var cakesBaked = 0

    while (cakesEaten < 5){
        eatCake()
        cakesEaten ++
    }

    do {
        bakeCake()
        cakesBaked++
    } while (cakesBaked < cakesEaten)
}
