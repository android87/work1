package ControlFlow

fun main() {
    val cakes = listOf("carrot", "cheese", "chocolate")
    for (cake in cakes) {
        println("It's a $cake cake!")
    }
}