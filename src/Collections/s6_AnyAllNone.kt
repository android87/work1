package Collections

fun main () {
    val numbers = listOf(1, -2, 3, -4, 5, -6)            // 1
    println(numbers)
    val anyNegative = numbers.any { it < 0 }             // 2
    println(anyNegative)
    val anyGT6 = numbers.any { it > 6 }                  // 3
    println(anyGT6)
    println()
    val numbers2 = listOf(1, -2, 3, -4, 5, -6)            // 1
    println(numbers2)
    val allEven = numbers.all { it % 2 == 0 }            // 2
    println(allEven)
    val allLess6 = numbers.all { it < 6 }                // 3
    println(allLess6)
    println()
    val numbers3 = listOf(1, -2, 3, -4, 5, -6)            // 1
    println(numbers3)
    val allEven2 = numbers.none { it % 2 == 1 }           // 2
    println(allEven2)
    val allLess6x = numbers.none { it > 6 }               // 3
    println(allLess6x)
}