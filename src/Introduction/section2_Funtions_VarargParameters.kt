package org.kotlinlang.play.Introduction

fun main () {
    fun printAll(vararg  messages: String) {
        for (m in messages) println(m)
    }
    printAll("Hello", "Hallo", "Salut", "Hola", "Hi")

    fun printAllWithPrefix(vararg message: String, prefix: String) {
        for (m in message) println(prefix + m)
    }
    printAllWithPrefix(
            "Hello",
            "Hallo",
            "Salut",
            "Hola",
            "Hi",
            prefix = "Greeting"
    )
    fun log(vararg entries: String) {
        printAll(*entries)
    }
}