package org.kotlinlang.play.Introduction

fun main () {
    var neverNull: String? = "This can't be null"
    neverNull = null

    var nullable: String? ="You can keep a null"
    nullable = null

    var inferredNonNull: String? ="The compiler assumes non-null"
    inferredNonNull = null

    fun strLength(notNull: String): Int {
        return notNull.length
    }
    if (neverNull != null) {
        strLength(neverNull)
    }
    //strLength(nullable)
}